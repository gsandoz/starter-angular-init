import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';


const homeRoutes: Routes = [
  { path: 'home', component: HomeComponent },
 ];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      homeRoutes
    )
  ],
  declarations: [],
  exports: [
    RouterModule,
  ]
})
export class HomeRoutingModule { }
