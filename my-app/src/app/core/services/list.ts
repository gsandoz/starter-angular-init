import { Item } from '../../shared/models/item.model';

export const ITEMS: Item[] = [
  {
    id: '1000',
    name: 'Belvors diary',
    reference: '4572',
    state: 'A livrer'
  },
  {
    id: '1000',
    name: 'Mistrels diary',
    reference: '4352',
    state: 'A livrer'
  },{
    id: '856',
    name: 'Cinderelas diary',
    reference: '4502',
    state: 'En cours'
  },{
    id: '950',
    name: 'Gwendolines diary',
    reference: '4574',
    state: 'Livrée'
  },
]
;
