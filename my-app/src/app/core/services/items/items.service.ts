import { Injectable } from '@angular/core';
import { ITEMS } from '../list';
import { Item } from '../../../shared/models/item.model';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ItemsService {

  // private collection: Item[] = ITEMS; // si accès static à partir d'une constante
  // constructor() { }
  // getCollection(): Item[] {
  //    return this.collection;
  // }
  private itemsCollection: AngularFirestoreCollection<Item>; // Accès base Firebase
  collection$: Observable<Item[]>;
  constructor(private afs: AngularFirestore) {
    this.itemsCollection = afs.collection<Item>('collectionStore');
    this.setCollection(this.itemsCollection.valueChanges());
   }

  getCollection(): Observable<Item[]> {
    return this.collection$;
  }

  setCollection(collection: Observable<Item[]>) {
    this.collection$ = collection;
  }
}
