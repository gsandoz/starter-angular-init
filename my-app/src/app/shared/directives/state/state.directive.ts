import { Directive, Input, OnInit, HostBinding } from '@angular/core';
import { State } from '../../enums/state.enum';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnInit {
  @Input('appState') appState: State;
  // Decorator pour mapper l'élément class du composnt span avec la valeur retour de formatClass()
  @HostBinding('class') elementClass: string;
  constructor() {
    console.log(this.appState); // undefined car la variable n'est pas encore initialiséz
  }

  ngOnInit(): void {
    console.log(this.appState); // variable initialisée, on a bien les valeurs affichées dans la console
    this.elementClass = this.formatClass(this.appState);
  }

  private removeAccent(state: string): string {
    return state.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }

  private formatClass(state: string): string {
    // attention au back tilde pour mélanger expression de variable et texte
    return `state-${this.removeAccent(state).toLowerCase().replace(' ', '-')}`;
  }
}
