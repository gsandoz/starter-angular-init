export enum State {
  ALIVRER = 'A livrer',
  ENCOURS = 'En Cours',
  LIVREE = 'Livrée'
}
