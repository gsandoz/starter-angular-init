import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';

import { ListItemsComponent } from './containers/list-items/list-items.component';

const itemsRoutes: Routes = [
  { path: 'liste', component: ListItemsComponent },
 ];
@NgModule({
  imports: [
    CommonModule,
     RouterModule.forChild(
      itemsRoutes
    )
  ],
  declarations: [],
  exports: [
    RouterModule,
  ]
})
export class ItemsRoutingModule { }
