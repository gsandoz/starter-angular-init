import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../../../core/services/items/items.service';
import { Item } from '../../../shared/models/item.model';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss']
})
export class ListItemsComponent implements OnInit {

  // //Pour afficher les données statiques issues d'une constante
  // collection: Item[];
  // constructor(private itemsService: ItemsService) { }

  // ngOnInit() {
  //   this.collection = this.itemsService.getCollection();
  //   console.log(this.collection);
  // }

  // Pour afficher des données à partir de Firebase
  collection$: Observable<Item[]>;
  constructor(private itemsService: ItemsService) { }

  ngOnInit() {
    // //Pour afficher les données statiques issues d'une constante
    // this.collection = this.itemsService.getCollection();
    // console.log(this.collection);

    // Pour afficher des données à partir de Firebase
    this.collection$ = this.itemsService.getCollection();
  }
}
