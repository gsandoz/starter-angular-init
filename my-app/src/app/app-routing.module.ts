import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { HomeComponent } from './home/components/home/home.component';
import { ListItemsComponent } from './items/containers/list-items/list-items.component';
import { PageNotFoundComponent } from './page-not-found/component/page-not-found/page-not-found.component';
import { ItemsModule } from './items/items.module';

const appRoutes: Routes = [
  { path: 'items', loadChildren: 'app/items/items.module#ItemsModule' }, // lazyloading de module
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes,
    //  { enableTracing: true } // <-- debugging purposes only
    {preloadingStrategy: PreloadAllModules}, // optimisation pour le lazyloading des très gros modules
    )
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
