import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { HomeModule } from './home/home.module';
// import { ItemsModule } from './items/items.module'; // supprimé dès lors que l'on fait du lazyloading (cf. app-routing-module)
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageNotFoundModule } from './page-not-found/page-not-found.module';
import { SharedModule } from './shared/shared.module';

import { ItemsService } from './core/services/items/items.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
// import { environment } from '../environments/environment';
import { Router } from '@angular/router';
import { environment } from '../environments/environment.prod';


@NgModule({
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    NgbModule.forRoot(),
    HomeModule,
    // ItemsModule, // supprimé dès lors que l'on fait du lazyloading (cf. app-routing-module)
    AppRoutingModule,
    CoreModule,
    SharedModule,
    PageNotFoundModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    ItemsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(router: Router) {
    if (!environment.production) {
      console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
    }
  }
}
